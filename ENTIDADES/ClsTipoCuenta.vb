﻿Public Class ClsTipoCuenta
    Private _idTipoCuenta As Integer
    Public Property IdTipoCuenta() As Integer
        Get
            Return _idTipoCuenta
        End Get
        Set(ByVal value As Integer)
            _idTipoCuenta = value
        End Set
    End Property
    Private _tipo As String
    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property
End Class
