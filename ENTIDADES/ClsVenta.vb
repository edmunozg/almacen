﻿Public Class ClsVenta
    Private _idVenta As Integer
    Public Property IdVenta() As Integer
        Get
            Return _idVenta
        End Get
        Set(ByVal value As Integer)
            _idVenta = value
        End Set
    End Property
    Private _idAlmacen As ClsAlmacen
    Public Property IdAlmacen() As ClsAlmacen
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As ClsAlmacen)
            _idAlmacen = value
        End Set
    End Property
    Private _idPersona As ClsPersona
    Public Property IdPersona() As ClsPersona
        Get
            Return _idPersona
        End Get
        Set(ByVal value As ClsPersona)
            _idPersona = value
        End Set
    End Property
    Private _idTipoCuenta As ClsTipoCuenta
    Public Property IdTipoCuenta() As ClsTipoCuenta
        Get
            Return _idTipoCuenta
        End Get
        Set(ByVal value As ClsTipoCuenta)
            _idTipoCuenta = value
        End Set
    End Property
    Private _fecha As Date
    Public Property Fecha() As Date
        Get
            Return _fecha
        End Get
        Set(ByVal value As Date)
            _fecha = value
        End Set
    End Property
    Private _cantidad As Int64
    Public Property Cantidad() As Int64
        Get
            Return _cantidad
        End Get
        Set(ByVal value As Int64)
            _cantidad = value
        End Set
    End Property
    Private _articulo As String
    Public Property Articulo() As String
        Get
            Return _articulo
        End Get
        Set(ByVal value As String)
            _articulo = value
        End Set
    End Property
    Private _detalle As String
    Public Property Detalle() As String
        Get
            Return _detalle
        End Get
        Set(ByVal value As String)
            _detalle = value
        End Set
    End Property
    Private _notaVenta As Int64
    Public Property NotaVenta() As Int64
        Get
            Return _notaVenta
        End Get
        Set(ByVal value As Int64)
            _notaVenta = value
        End Set
    End Property
    Private _factura As Int64
    Public Property Factura() As Int64
        Get
            Return _factura
        End Get
        Set(ByVal value As Int64)
            _factura = value
        End Set
    End Property
    Private _valorTotal As Decimal
    Public Property ValorTotal() As Decimal
        Get
            Return _valorTotal
        End Get
        Set(ByVal value As Decimal)
            _valorTotal = value
        End Set
    End Property
End Class
