﻿Public Class ClsAlmacen
    Private _idAlmacen As Integer
    Public Property IdAlmacen() As Integer
        Get
            Return _idAlmacen
        End Get
        Set(ByVal value As Integer)
            _idAlmacen = value
        End Set
    End Property
    Private _almacen As String
    Public Property Almacen() As String
        Get
            Return _almacen
        End Get
        Set(ByVal value As String)
            _almacen = value
        End Set
    End Property
End Class
