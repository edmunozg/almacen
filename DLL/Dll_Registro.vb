﻿Imports MySql.Data.MySqlClient
Public Class Dll_Registro
    Inherits Dll_Conexion
    Public Function registros(ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "registros"
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function filtrarPorCodigo(ByVal value As String, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "registros_filtrarPorCodigo"
        command.Parameters.AddWithValue("_codigo", value)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function filtrarPorCedula(ByVal value As String, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "registros_filtrarPorCedula"
        command.Parameters.AddWithValue("_cedula", value)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "registros_filtrarPorApellido"
        command.Parameters.AddWithValue("_apellido", value)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
End Class
