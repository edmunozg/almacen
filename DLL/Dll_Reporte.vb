﻿Imports MySql.Data.MySqlClient
Public Class Dll_Reporte
    Inherits Dll_Conexion
    Function reporteRegistroPorFecha(ByVal date1 As Date, ByVal date2 As Date, ByRef mensaje As String) As DataSet
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "reporte_RegistroPorFecha"
        command.Parameters.AddWithValue("_fechaInicio", date1)
        command.Parameters.AddWithValue("_fechaFin", date2)
        command.Connection = connection
        Dim ds As New DataSet
        Dim da As New MySqlDataAdapter
        da.SelectCommand = command
        Try
            connection.Open()
            da.Fill(ds, "dsRegistro")
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If connection.State = ConnectionState.Open Then
                connection.Close()
                command.Parameters.Clear()
            End If
        End Try
        Return ds
    End Function
End Class
