﻿Imports ENTIDADES
Imports MySql.Data.MySqlClient
Public Class Dll_Venta
    Inherits Dll_Conexion
    Public Function ingresarBD(ByVal value As ClsVenta, ByRef mensaje As String) As Boolean
        Me.Conexion()
        Dim command As New MySqlCommand
        Dim estado As Boolean = False
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "venta_insert"
        command.Parameters.AddWithValue("_idAlmacen", value.IdAlmacen.IdAlmacen)
        command.Parameters.AddWithValue("_idPersona", value.IdPersona.IdPersona)
        command.Parameters.AddWithValue("_idTipoCuenta", value.IdTipoCuenta.IdTipoCuenta)
        command.Parameters.AddWithValue("_fecha", value.Fecha)
        command.Parameters.AddWithValue("_cantidad", value.Cantidad)
        command.Parameters.AddWithValue("_articulo", value.Articulo)
        command.Parameters.AddWithValue("_detalle", value.Detalle)
        command.Parameters.AddWithValue("_notaVenta", value.NotaVenta)
        command.Parameters.AddWithValue("_factura", value.Factura)
        command.Parameters.AddWithValue("_valorTotal", value.ValorTotal)
        Try
            command.Connection = Me.connection
            Me.connection.Open()
            command.ExecuteNonQuery()
            estado = True
            mensaje = "Ingresado Exitosamente!"
        Catch ex As Exception
            estado = False
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return estado
    End Function
    Public Function modificarBD(ByVal value As ClsVenta, ByRef mensaje As String) As Boolean
        Me.Conexion()
        Dim command As New MySqlCommand
        Dim estado As Boolean = False
        command.CommandText = "venta_update"
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("_idVenta", value.IdVenta)
        command.Parameters.AddWithValue("_idAlmacen", value.IdAlmacen.IdAlmacen)
        command.Parameters.AddWithValue("_idPersona", value.IdPersona.IdPersona)
        command.Parameters.AddWithValue("_idTipoCuenta", value.IdTipoCuenta.IdTipoCuenta)
        command.Parameters.AddWithValue("_fecha", value.Fecha)
        command.Parameters.AddWithValue("_cantidad", value.Cantidad)
        command.Parameters.AddWithValue("_articulo", value.Articulo)
        command.Parameters.AddWithValue("_detalle", value.Detalle)
        command.Parameters.AddWithValue("_notaVenta", value.NotaVenta)
        command.Parameters.AddWithValue("_factura", value.Factura)
        command.Parameters.AddWithValue("_valorTotal", value.ValorTotal)
        Try
            command.Connection = Me.connection
            Me.connection.Open()
            command.ExecuteNonQuery()
            mensaje = "Modificado Exitosamente!"
            estado = True
        Catch ex As Exception
            estado = False
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return estado
    End Function
    Public Function selectBD(ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "venta_select"
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function consultarPorId(ByVal id As Integer, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "venta_consultarPorId"
        command.Parameters.AddWithValue("_idVenta", id)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "venta_filtrarPorApellido"
        command.Parameters.AddWithValue("_apellido", value)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
End Class
