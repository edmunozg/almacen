﻿Imports ENTIDADES
Imports MySql.Data.MySqlClient
Public Class Dll_Persona
    Inherits Dll_Conexion
    Public Function ingresarBD(ByVal value As ClsPersona, ByRef mensaje As String) As Boolean
        Me.Conexion()
        Dim command As New MySqlCommand
        Dim estado As Boolean = False
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "persona_insert"
        command.Parameters.AddWithValue("_cedula", value.Cedula)
        command.Parameters.AddWithValue("_nombre", value.Nombre)
        command.Parameters.AddWithValue("_apellido", value.Apellido)
        command.Parameters.AddWithValue("_direccion", value.Direccion)
        command.Parameters.AddWithValue("_telefono", value.Telefono)
        command.Parameters.AddWithValue("_codigo", value.Codigo)
        Try
            command.Connection = Me.connection
            Me.connection.Open()
            command.ExecuteNonQuery()
            estado = True
            mensaje = "Ingresado Exitosamente!"
        Catch ex As Exception
            estado = False
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return estado
    End Function
    Public Function modificarBD(ByVal value As ClsPersona, ByRef mensaje As String) As Boolean
        Me.Conexion()
        Dim command As New MySqlCommand
        Dim estado As Boolean = False
        command.CommandText = "persona_update"
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("_idPersona", value.IdPersona)
        command.Parameters.AddWithValue("_cedula", value.Cedula)
        command.Parameters.AddWithValue("_nombre", value.Nombre)
        command.Parameters.AddWithValue("_apellido", value.Apellido)
        command.Parameters.AddWithValue("_direccion", value.Direccion)
        command.Parameters.AddWithValue("_telefono", value.Telefono)
        command.Parameters.AddWithValue("_codigo", value.Codigo)
        Try
            command.Connection = Me.connection
            Me.connection.Open()
            command.ExecuteNonQuery()
            mensaje = "Modificado Exitosamente!"
            estado = True
        Catch ex As Exception
            estado = False
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return estado
    End Function
    Public Function selectBD(ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "persona_select"
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function consultarPorId(ByVal id As Integer, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "persona_consultarPorId"
        command.Parameters.AddWithValue("_idPersona", id)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
    Public Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Me.Conexion()
        Dim command As New MySqlCommand
        command.CommandType = CommandType.StoredProcedure
        command.CommandText = "persona_filtrarPorApellido"
        command.Parameters.AddWithValue("_apellido", value)
        Dim da As New MySqlDataAdapter
        Dim dt As New DataTable
        command.Connection = Me.connection
        da.SelectCommand = command
        Try
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                dt = Nothing
                mensaje = "No existen registros."
            End If
        Catch ex As Exception
            dt = Nothing
            mensaje = ex.Message
        Finally
            If Me.connection.State = ConnectionState.Open Then
                Me.connection.Close()
            End If
        End Try
        Return dt
    End Function
End Class
