CREATE DATABASE  IF NOT EXISTS `db_registro` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_registro`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: db_registro
-- ------------------------------------------------------
-- Server version	5.5.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `almacen`
--

DROP TABLE IF EXISTS `almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacen` (
  `idAlmacen` bigint(20) NOT NULL AUTO_INCREMENT,
  `almacen` varchar(100) NOT NULL,
  PRIMARY KEY (`idAlmacen`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacen`
--

LOCK TABLES `almacen` WRITE;
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;
INSERT INTO `almacen` VALUES (1,'BASTIÓN'),(2,'PARAISO');
/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idPersona` bigint(20) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `codigo` bigint(20) NOT NULL,
  PRIMARY KEY (`idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'0959106345','NORBERTO ANDRES','LAJE ALVARADO','Bastion Popular Mz 647 Sl 14','0959106345',1953),(2,'0926665050','ANGEL CARLOS','SOLIS ESPINOZA','FLOR DE BASTION BL:22 MZ:1354  SL:5','0985822487',1954),(3,'0917564262','MAXIMILIANO','SILVERA GOMEZ','BASTION POPULAR BL1B MZ:482 SL:2','0994467862',1956),(4,'0968177463','JONNATHAN MANUEL','MORAN ACOSTA','FLOR DE BASTION BL:2 MZ:1387 SL:8','0968177463',968177463),(5,'0940835119','JULIO EDUARDO','RIZO SEVILLANO','BASTION POPULAR BL:1-A MZ:485 SL:49','0959220014',1958),(6,'0955522891','PABLA PASCUALA','FIGUEROA MERO','BASTION POPULAR  MZ:1703 SL:32A COOP. EL TREBOL','',2053),(7,'0922921317','GEOCONDA ELIZABETH','ESPINALES  PILAY','BASTION POPULAR MZ:518 SL:16','0998523371',2054),(8,'1203224199','JACINTA PETITA','ALVAREZ GARCIA','BASTION POPULAR BL:4 MZ:A SL:1','0993836565',2055),(9,'1311341513','INTRIAGO CEVALLOS JOSE ADALBERTO','apellido','BASTION POPULAR BL:1A MZ:665  SL:8','0990637185',2056),(10,'0916653884','ULLOA  MUENTES KARINA AUXILIADORA ','apellido','COOP. NUEVA GUAYAQUIL MZ:2408 SL:4','0997432900',2057),(11,'1707447205','URBAN PARRALES WELLINTON OSWALDO','apellido','','',2058),(12,'0912647310','CERRANO CHOEZ REYES PILAR','apellido','BASTION POPULAR BL:2 MZ:646 SL:6','0950384347',2059),(13,'0925516338','ROSADO QUIROZ WASHINGTON GABRIEL','apellido','Ciudad Victoria 2 MZ:4553 ','0988506458',2060),(14,'1301482764','SANCHEZ  MARIA','apellido','BASTION POPULAR ','',2061),(15,'0918040775','JESSICA MERCEDES','PRECIADO ANCHUNDIA','m','0981208984',2062),(16,'0911340115','SANCHEZ PARRALES FELIPE SANTIAGO','apellido','MONTE SINAY COOP. LA MARIANITA ','',2063),(17,'130399781','CHELE QUIMIS ANGELA PETRONILLA ','apellido','BASTION POPULAR BL:2 MZ:676','0990180997',2064),(18,'0601728785','LEMA GUAMAN JOSE','apellido','','0',2065),(19,'0923408983','CAJAPE QUIJIJE JUAN XAVIER','apellido','COOP. VALERIO ESTACIO MZ:3161 SL:2 BL:7','0939991891',2066),(20,'0923345011',' FIGUEROA ARTEAGA NELSON','apellido','BASTION POPULAR BL1A MZ:1674 SL:3','',2067),(21,'1720782471','LAZ ALCIVAR RAMON GEOVANNY','apellido','COOP. ASSAD BUCARAM MZ:339 SL:1','',2068),(22,'0914056270','RODOLFO FRANCISCO','CASTRO BAJAÑA','COOP. ROMERO ARROBA','0985633447',2069),(23,'1704545023','JULIO','CARRILLO BUÑAY ','BASTION POPULAR BL:1A MZ:52 SL:1',NULL,2070),(24,'0955379607','GUARANDA MARCILLO MARLON PATRICIO','apellido','MONTE SINAI','',2071),(25,'0922283197','PACHECO CHAVARRIA VILMA ISABEL','apellido','PARAISO DE LA FLOR BL:6 MZ:320 SL: 9','097832762',2072),(26,'0604310268','SINALUISA SINALUISA FAUSTO RAUL','apellido','BASTION POPULAR ','',2073),(27,'0916829054','ZAMBRANO DEMERA JENNY JOLANDA ','apellido','FLOR DE BASTION  BL:16 MZ:1211 SL:13','0985633447',2074),(28,'0912955978','FAJARDO TORRES YOCONDA LUCIA','apellido','BASTION POPULAR BL:1A MZ:474 SL:19','',2075),(29,'1302600760','LOOR SANCON SANTA EMERCIANA','apellido','','',2076),(31,'0930347737','ESTEBAN DANIEL','MUÑOZ GUEVARA','BASTION POPULAR BQ 1A MZ 580 SL 11','0959011294',0);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocuenta`
--

DROP TABLE IF EXISTS `tipocuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocuenta` (
  `idTipoCuenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`idTipoCuenta`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocuenta`
--

LOCK TABLES `tipocuenta` WRITE;
/*!40000 ALTER TABLE `tipocuenta` DISABLE KEYS */;
INSERT INTO `tipocuenta` VALUES (1,'CRÉDITO'),(2,'OBSEQUIO'),(3,'COMBO');
/*!40000 ALTER TABLE `tipocuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `idVenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `idAlmacen` bigint(20) NOT NULL,
  `idPersona` bigint(20) NOT NULL,
  `idTipoCuenta` bigint(20) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `articulo` varchar(200) DEFAULT NULL,
  `detalle` varchar(200) DEFAULT NULL,
  `notaVenta` bigint(20) DEFAULT NULL,
  `factura` bigint(20) DEFAULT NULL,
  `valorTotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idVenta`),
  KEY `fk_venta_almacen_idx` (`idAlmacen`),
  KEY `fk_venta_tipoCuenta1_idx` (`idTipoCuenta`),
  KEY `fk_venta_persona1_idx` (`idPersona`),
  CONSTRAINT `fk_venta_almacen` FOREIGN KEY (`idAlmacen`) REFERENCES `almacen` (`idAlmacen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_tipoCuenta1` FOREIGN KEY (`idTipoCuenta`) REFERENCES `tipocuenta` (`idTipoCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,1,1,1,'2013-01-12',1,'DW- DEC-32R1U','LCD 32\" DEFINITION/USB/HDMI DAEWOO',0,0,810.00),(2,1,1,2,'2014-12-01',1,'LG- DP122_NG','LG- DP122_NG',0,0,0.00),(3,1,3,1,'2013-01-12',1,'PARLANTE BZK15A','PARLANTE AMPLIFICADO BZK15A',0,0,308.00),(4,1,4,1,'2014-10-09',1,'CEL- NOKIA -201','TELEFONO NOKIA 201GRIS',0,0,280.00),(5,1,5,2,'2014-10-09',1,'OBS- TABLA DE PLANCHAR','TABLA DE PLANCHAR',0,0,0.00),(6,1,23,1,'2014-01-03',1,'GB- ESPAnOLA','COCINA ESPAnOLA BL GLOBAL ESP005',0,0,630.00),(7,1,18,1,'2014-01-03',1,'PAR- CL-ANIVERSARIO-105','COLCHON ANIVERSARIO 105x190x0',0,0,156.00),(8,1,26,1,'2014-01-07',1,'CEL- NOKIA -303','TELEFONO NOKIA ASHA 303NEGRO',0,0,294.91),(9,1,16,1,'2014-01-04',1,'CEL- NOKIA-311','TELEFONO NOKIA ASHA 311',0,0,345.18),(10,1,12,1,'2014-01-04',1,'DX- CDE30CBX-0','COCINA 76 DUREX BLANCO 30CBX-0',0,0,1062.00),(11,1,28,2,'2014-01-04',1,'PAR- CL-ANIVERSARIO-105','COLCHON ANIVERSARIO 105x190x0',0,0,0.00),(12,1,22,2,'2014-01-04',1,'BMX_JORDY 12','BICICLETA _BMX_JORDY 12',0,0,0.00),(13,1,10,2,'2014-01-04',1,'OBS- JGO DE VAJILLAS 20','JUEGOS DE VAJILLAS 20 PIEZAS',0,0,0.00),(14,1,10,2,'2014-01-04',1,'OBS- JGO DE JARRA ','JUEGO DE JARRA CON VASOS 7 PIEZAS ',0,0,0.00),(15,1,10,2,'2014-01-04',1,'OBS- TOLDO MOSQUITERO','TOLDO MOSQUITERO 4 PUNTAS VELO SUIZO ',0,0,0.00),(16,1,10,2,'2014-01-04',1,'OBS- TABLA DE PLANCHAR','TABLA DE PLANCHAR',0,0,0.00),(17,1,10,1,'2014-01-09',1,'','',0,0,85.00),(18,1,10,1,'2014-01-09',1,'CP- LAPTOP_SVF14211CL','LAPTOP MARCITECH_SONY_SVF14211CL (INTEL P.1.80GZ-4GbRAM)+IMPRESORA',0,0,946.10),(19,1,10,1,'2014-01-10',1,'SN- KDL-32R435','TV. LED_SONY_NEGRO KDL-32R435(32\"HD)',0,0,1030.39),(20,1,27,1,'2014-01-10',1,'MB- LMA160BZPO','LAVADORA MABE AUT. 16 LB',0,0,563.00),(21,1,15,1,'2014-01-14',1,'DH- ROPERO EN L',' ROPERO EN L',0,0,487.00),(22,1,15,3,'2014-01-14',1,'PAR- CL-ANIVERSARIO-135','COLCHON ANIVERSARIO 135x190x0',0,0,0.00),(23,1,25,1,'2014-01-14',1,'DX- RDE7170FYJ','REFRIGERADORA DUREX GRAFITO 7170',0,0,936.00),(24,1,7,1,'2014-01-18',1,'DX- LDD304BO','LAVADORA  SEMIAUTOMATICA TICA_DUREX',0,0,529.47),(25,1,20,1,'2014-01-15',1,'','',0,0,1148.40),(26,1,19,1,'2014-01-18',1,'MB- RMT346VJEE','REFIRGERADORA MABE POLAR',0,0,824.98),(27,1,13,1,'2014-01-18',1,'LG- DP122','DVD_LG_DP122_NG',0,0,78.00),(28,1,24,1,'2014-01-18',1,'DX- CDE30CBX-0','COCINA 76 DUREX BLANCO 30CBX-0',0,0,751.06),(29,1,6,1,'2014-01-18',1,'','',0,0,329.88),(30,1,8,1,'2014-01-18',1,'RB-DEH16V-RADIO PIONEER','RB-RADIO PIONEER',0,0,249.00),(31,1,14,1,'2014-01-18',1,'CM- BOX-135','CAMA BASE 135*190',0,0,300.00),(32,1,29,1,'2014-01-21',1,'','',0,0,55.00),(33,1,17,1,'2014-01-23',1,'CP- HP-455','PORTATIL HP 455 AMD DISCO DURO 500 GB',0,0,996.30),(34,1,9,1,'2014-01-24',1,'CEL- NOKIA -501','TELEFONO NOKIA ASHA 501NEGRO',0,0,320.00),(35,1,11,1,'2014-01-25',1,'DX- CDE30CBX-0','COCINA 76 DUREX BLANCO 30CBX-0',0,0,738.90),(36,1,21,1,'2014-01-27',1,'DX- LDD304BO','LAVADORA  SEMIAUTOMATICA TICA_DUREX',0,0,331.24);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'db_registro'
--
/*!50003 DROP PROCEDURE IF EXISTS `almacen_consultarPorId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `almacen_consultarPorId`(IN _idAlmacen BIGINT)
BEGIN
SELECT * FROM almacen WHERE idAlmacen=_idAlmacen;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `almacen_filtrarPorAlmacen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `almacen_filtrarPorAlmacen`(IN _almacen VARCHAR(100))
BEGIN
SELECT idAlmacen AS Id, almacen AS Almacen  
FROM almacen
WHERE almacen like CONCAT('%',_almacen,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `almacen_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `almacen_insert`(
IN _almacen varchar(100)
)
BEGIN
INSERT INTO almacen (almacen)
VALUES (_almacen);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `almacen_select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `almacen_select`()
BEGIN
SELECT * FROM almacen;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `almacen_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `almacen_update`(IN _idAlmacen BIGINT,IN _almacen VARCHAR(100))
BEGIN
UPDATE almacen
SET almacen = _almacen
WHERE idAlmacen = _idAlmacen;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `persona_consultarPorId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `persona_consultarPorId`(IN _idPersona BIGINT)
BEGIN
SELECT * FROM persona WHERE idPersona=_idPersona;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `persona_filtrarPorApellido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `persona_filtrarPorApellido`(IN _apellido VARCHAR(200))
BEGIN
SELECT idPersona AS Id, codigo AS Codigo,cedula AS Cedula ,apellido AS Apellido ,nombre AS Nombre
FROM persona
WHERE apellido like CONCAT('%',_apellido,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `persona_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `persona_insert`(
IN _cedula VARCHAR(10),
IN _nombre VARCHAR(50),
IN _apellido VARCHAR(50),
IN _direccion VARCHAR(200),
IN _telefono VARCHAR(45),
IN _codigo BIGINT
)
BEGIN
INSERT INTO persona (cedula,nombre,apellido,direccion,telefono,codigo)
VALUES (_cedula,_nombre,_apellido,_direccion,_telefono,_codigo);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `persona_select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `persona_select`()
BEGIN
SELECT idPersona,CONCAT(apellido,' ',nombre) AS nombre FROM persona;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `persona_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `persona_update`(
IN _idPersona BIGINT,
IN _cedula VARCHAR(10),
IN _nombre VARCHAR(50),
IN _apellido VARCHAR(50),
IN _direccion VARCHAR(200),
IN _telefono VARCHAR(45),
IN _codigo BIGINT
)
BEGIN
UPDATE persona
SET cedula = _cedula,
	nombre = _nombre,
	apellido = _apellido,
	direccion = _direccion,
	telefono = _telefono,
	codigo = _codigo
WHERE idPersona = _idPersona;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registros` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registros`()
BEGIN
SELECT v.idVenta AS Id,a.idAlmacen,p.idPersona,tc.idTipoCuenta,a.almacen AS ALMACEN,v.fecha AS FECHA,p.cedula AS CEDULA,
CONCAT(p.apellido,' ',p.nombre) AS NOMBRE,p.direccion AS DIRECCION,p.telefono AS TELEFONO,
p.codigo AS CODIGO,v.cantidad AS CANTIDAD,v.articulo AS ARTICULO,v.detalle AS DETALLE,
tc.tipo AS TIPO_DE_CUENTA,v.notaVenta AS NOTA_VENTA,v.factura AS FACTURA,v.valortotal AS VALOR_TOTAL
FROM venta AS v
INNER JOIN persona AS p
ON v.idPersona = p.idPersona
INNER JOIN almacen AS a
ON v.idAlmacen = a.idAlmacen
INNER JOIN tipocuenta as tc
ON v.idTipoCuenta = tc.idTipoCuenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registros_filtrarPorApellido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registros_filtrarPorApellido`(IN _apellido VARCHAR(45) )
BEGIN
SELECT v.idVenta AS Id,a.idAlmacen,p.idPersona,tc.idTipoCuenta,a.almacen AS ALMACEN,v.fecha AS FECHA,p.cedula AS CEDULA,
CONCAT(p.apellido ,' ',p.nombre) AS NOMBRE,p.direccion AS DIRECCION,p.telefono AS TELEFONO,
p.codigo AS CODIGO,v.cantidad AS CANTIDAD,v.articulo AS ARTICULO,v.detalle AS DETALLE,
tc.tipo AS TIPO_DE_CUENTA,v.notaVenta AS NOTA_VENTA,v.factura AS FACTURA,v.valortotal AS VALOR_TOTAL
FROM venta AS v
INNER JOIN persona AS p
ON v.idPersona = p.idPersona
INNER JOIN almacen AS a
ON v.idAlmacen = a.idAlmacen
INNER JOIN tipocuenta as tc
ON v.idTipoCuenta = tc.idTipoCuenta
WHERE p.apellido like CONCAT('%',_apellido,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registros_filtrarPorCedula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registros_filtrarPorCedula`(IN _cedula VARCHAR(10))
BEGIN
SELECT v.idVenta AS Id,a.idAlmacen,p.idPersona,tc.idTipoCuenta,a.almacen AS ALMACEN,v.fecha AS FECHA,p.cedula AS CEDULA,
CONCAT(p.apellido,' ',p.nombre) AS NOMBRE,p.direccion AS DIRECCION,p.telefono AS TELEFONO,
p.codigo AS CODIGO,v.cantidad AS CANTIDAD,v.articulo AS ARTICULO,v.detalle AS DETALLE,
tc.tipo AS TIPO_DE_CUENTA,v.notaVenta AS NOTA_VENTA,v.factura AS FACTURA,v.valortotal AS VALOR_TOTAL
FROM venta AS v
INNER JOIN persona AS p
ON v.idPersona = p.idPersona
INNER JOIN almacen AS a
ON v.idAlmacen = a.idAlmacen
INNER JOIN tipocuenta as tc
ON v.idTipoCuenta = tc.idTipoCuenta
WHERE p.cedula like CONCAT('%',_cedula,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `registros_filtrarPorCodigo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `registros_filtrarPorCodigo`(IN _codigo VARCHAR(45))
BEGIN
SELECT v.idVenta AS Id,a.idAlmacen,p.idPersona,tc.idTipoCuenta,a.almacen AS ALMACEN,v.fecha AS FECHA,p.cedula AS CEDULA,
CONCAT(p.apellido,' ',p.nombre) AS NOMBRE,p.direccion AS DIRECCION,p.telefono AS TELEFONO,
p.codigo AS CODIGO,v.cantidad AS CANTIDAD,v.articulo AS ARTICULO,v.detalle AS DETALLE,
tc.tipo AS TIPO_DE_CUENTA,v.notaVenta AS NOTA_VENTA,v.factura AS FACTURA,v.valortotal AS VALOR_TOTAL
FROM venta AS v
INNER JOIN persona AS p
ON v.idPersona = p.idPersona
INNER JOIN almacen AS a
ON v.idAlmacen = a.idAlmacen
INNER JOIN tipocuenta as tc
ON v.idTipoCuenta = tc.idTipoCuenta
WHERE p.codigo like CONCAT('%',_codigo,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reporte_RegistroPorFecha` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_RegistroPorFecha`(IN _fechaInicio DATE,IN _fechaFin DATE)
BEGIN
SELECT a.almacen AS ALMACEN,v.fecha AS FECHA,p.cedula AS CEDULA,
CONCAT(p.apellido,' ',p.nombre) AS NOMBRE,p.direccion AS DIRECCION,p.telefono AS TELEFONO,
p.codigo AS CODIGO,v.cantidad AS CANTIDAD,v.articulo AS ARTICULO,v.detalle AS DETALLE,
tc.tipo AS TIPO_DE_CUENTA,v.notaVenta AS NOTA_VENTA,v.factura AS FACTURA,v.valortotal AS VALOR_TOTAL
FROM venta AS v
INNER JOIN persona AS p
ON v.idPersona = p.idPersona
INNER JOIN almacen AS a
ON v.idAlmacen = a.idAlmacen
INNER JOIN tipocuenta as tc
ON v.idTipoCuenta = tc.idTipoCuenta
WHERE v.fecha BETWEEN _fechaInicio AND _fechaFin ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tipoCuenta_consultarPorId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tipoCuenta_consultarPorId`(IN _idTipoCuenta BIGINT)
BEGIN
SELECT * FROM tipocuenta WHERE idTipoCuenta=_idTipoCuenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tipoCuenta_filtrarPorTipo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tipoCuenta_filtrarPorTipo`(IN _tipo VARCHAR(50))
BEGIN
SELECT idTipoCuenta AS Id, tipo AS Tipo  
FROM tipocuenta
WHERE tipo like CONCAT('%',_tipo,'%') ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tipoCuenta_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tipoCuenta_insert`(IN _tipo VARCHAR(50))
BEGIN
INSERT INTO tipocuenta(tipo)
VALUES (_tipo);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tipoCuenta_select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tipoCuenta_select`()
BEGIN
SELECT * FROM tipocuenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tipoCuenta_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tipoCuenta_update`(IN _idTipoCuenta BIGINT, IN _tipo VARCHAR(50))
BEGIN
UPDATE tipocuenta
SET tipo = _tipo
WHERE idTipoCuenta = _idTipoCuenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `venta_consultarPorId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `venta_consultarPorId`(IN _idVenta BIGINT)
BEGIN
SELECT * FROM venta WHERE idVenta=_idVenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `venta_filtrarPorApellido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `venta_filtrarPorApellido`(IN _apellido VARCHAR(50))
BEGIN
SELECT v.idVenta AS Id, p.codigo AS Codigo, p.cedula AS Cedula, CONCAT( p.apellido,' ',p.nombre) AS Nombre ,v.articulo AS Articulo,v.detalle AS Detalle,v.valorTotal AS ValorTotal
FROM persona AS p
INNER JOIN venta AS v
ON p.idPersona = v.idPersona
WHERE p.apellido LIKE CONCAT('%',_apellido,'%');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `venta_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `venta_insert`(
IN _idAlmacen BIGINT,
IN _idPersona BIGINT,
IN _idTipoCuenta BIGINT,
IN _fecha DATE,
IN _cantidad BIGINT,
IN _articulo VARCHAR(200),
IN _detalle VARCHAR(200),
IN _notaVenta BIGINT,
IN _factura BIGINT,
IN _valorTotal DECIMAL(10,2)
)
BEGIN
INSERT INTO venta (idAlmacen,idPersona,idTipoCuenta,fecha,cantidad,articulo,detalle,notaVenta,factura,valorTotal)
VALUES (_idAlmacen,_idPersona,_idTipoCuenta,_fecha,_cantidad,_articulo,_detalle,_notaVenta,_factura,_valorTotal);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `venta_select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `venta_select`()
BEGIN
SELECT * FROM venta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `venta_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `venta_update`(
IN _idVenta BIGINT,
IN _idAlmacen BIGINT,
IN _idPersona BIGINT,
IN _idTipoCuenta BIGINT,
IN _fecha DATE,
IN _cantidad BIGINT,
IN _articulo VARCHAR(200),
IN _detalle VARCHAR(200),
IN _notaVenta BIGINT,
IN _factura BIGINT,
IN _valorTotal DECIMAL(10,2)
)
BEGIN
UPDATE venta
SET idAlmacen = _idAlmacen,
	idPersona = _idPersona,
	idTipoCuenta = _idTipoCuenta,
	fecha = _fecha,
	cantidad = _cantidad,
	articulo =_articulo,
	detalle = _detalle,
	notaVenta = _notaVenta,
	factura =_factura,
	valorTotal = _valorTotal
WHERE idVenta = _idVenta;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-17 15:04:28
