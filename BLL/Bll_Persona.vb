﻿Imports ENTIDADES
Imports DLL
Public Class Bll_Persona
    Shared Function ingresarBD(ByVal value As ClsPersona, ByRef mensaje As String) As Boolean
        Return New Dll_Persona().ingresarBD(value, mensaje)
    End Function

    Shared Function modificarBD(ByVal value As ClsPersona, ByRef mensaje As String) As Boolean
        Return New Dll_Persona().modificarBD(value, mensaje)
    End Function

    Shared Function selectBD(ByRef mensaje As String) As DataTable
        Return New Dll_Persona().selectBD(mensaje)
    End Function

    Shared Function consultarPorId(ByVal id As String, ByRef mensaje As String) As DataTable
        Return New Dll_Persona().consultarPorId(id, mensaje)
    End Function

    Shared Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Persona().filtrarPorApellido(value, mensaje)
    End Function
End Class
