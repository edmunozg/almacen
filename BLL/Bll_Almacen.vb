﻿Imports ENTIDADES
Imports DLL
Public Class Bll_Almacen
    Shared Function ingresarBD(ByVal value As ClsAlmacen, ByRef mensaje As String) As Boolean
        Return New Dll_Almacen().ingresarBD(value, mensaje)
    End Function

    Shared Function modificarBD(ByVal value As ClsAlmacen, ByRef mensaje As String) As Boolean
        Return New Dll_Almacen().modificarBD(value, mensaje)
    End Function

    Shared Function selectBD(ByRef mensaje As String) As DataTable
        Return New Dll_Almacen().selectBD(mensaje)
    End Function

    Shared Function consultarPorId(ByVal id As String, ByRef mensaje As String) As DataTable
        Return New Dll_Almacen().consultarPorId(id, mensaje)
    End Function


    Shared Function filtrarPorAlmacen(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Almacen().filtrarPorAlmacen(value, mensaje)
    End Function
End Class
