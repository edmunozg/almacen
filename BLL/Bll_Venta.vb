﻿Imports ENTIDADES
Imports DLL
Public Class Bll_Venta
    Shared Function ingresarBD(ByVal value As ClsVenta, ByRef mensaje As String) As Boolean
        Return New Dll_Venta().ingresarBD(value, mensaje)
    End Function

    Shared Function modificarBD(ByVal value As ClsVenta, ByRef mensaje As String) As Boolean
        Return New Dll_Venta().modificarBD(value, mensaje)
    End Function

    Shared Function selectBD(ByRef mensaje As String) As DataTable
        Return New Dll_Venta().selectBD(mensaje)
    End Function

    Shared Function consultarPorId(ByVal id As String, ByRef mensaje As String) As DataTable
        Return New Dll_Venta().consultarPorId(id, mensaje)
    End Function

    Shared Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Venta().filtrarPorApellido(value, mensaje)
    End Function
End Class
