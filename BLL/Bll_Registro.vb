﻿Imports DLL
Public Class Bll_Registro
    Shared Function registros(ByRef mensaje As String) As DataTable
        Return New Dll_Registro().registros(mensaje)
    End Function
    Shared Function filtrarPorCodigo(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Registro().filtrarPorCodigo(value, mensaje)
    End Function
    Shared Function filtrarPorCedula(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Registro().filtrarPorCedula(value, mensaje)
    End Function
    Shared Function filtrarPorApellido(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_Registro().filtrarPorApellido(value, mensaje)
    End Function
End Class
