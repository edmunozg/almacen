﻿Imports ENTIDADES
Imports DLL
Public Class Bll_TipoCuenta
    Shared Function ingresarBD(ByVal value As ClsTipoCuenta, ByRef mensaje As String) As Boolean
        Return New Dll_TipoCuenta().ingresarBD(value, mensaje)
    End Function

    Shared Function modificarBD(ByVal value As ClsTipoCuenta, ByRef mensaje As String) As Boolean
        Return New Dll_TipoCuenta().modificarBD(value, mensaje)
    End Function

    Shared Function selectBD(ByRef mensaje As String) As DataTable
        Return New Dll_TipoCuenta().selectBD(mensaje)
    End Function

    Shared Function consultarPorId(ByVal id As String, ByRef mensaje As String) As DataTable
        Return New Dll_TipoCuenta().consultarPorId(id, mensaje)
    End Function


    Shared Function filtrarPorTipo(ByVal value As String, ByRef mensaje As String) As DataTable
        Return New Dll_TipoCuenta().filtrarPorTipo(value, mensaje)
    End Function
End Class
