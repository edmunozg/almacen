﻿Public Class FrmPrincipal
    Private Sub VentaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VentaToolStripMenuItem.Click
        FrmVenta.MdiParent = Me
        FrmVenta.MaximizeBox = False
        FrmVenta.MinimizeBox = False
        FrmVenta.Show()
    End Sub
    Private Sub AlmacenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AlmacenToolStripMenuItem.Click
        FrmAlmacen.MdiParent = Me
        FrmAlmacen.MaximizeBox = False
        FrmAlmacen.MinimizeBox = False
        FrmAlmacen.Show()
    End Sub
    Private Sub PersonaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PersonaToolStripMenuItem.Click
        FrmPersona.MdiParent = Me
        FrmPersona.MaximizeBox = False
        FrmPersona.MinimizeBox = False
        FrmPersona.Show()
    End Sub
    Private Sub TipoCuentaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TipoCuentaToolStripMenuItem.Click
        FrmTipoCuenta.MdiParent = Me
        FrmTipoCuenta.MaximizeBox = False
        FrmTipoCuenta.MinimizeBox = False
        FrmTipoCuenta.Show()
    End Sub
    Private Sub RegistrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegistrosToolStripMenuItem.Click
        FrmRegistro.MdiParent = Me
        FrmRegistro.Show()
    End Sub

    Private Sub ReporteVentaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ReporteVentaToolStripMenuItem1.Click
        FrmReporteRegistro.MdiParent = Me
        FrmReporteRegistro.Show()
    End Sub
End Class