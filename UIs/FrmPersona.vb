﻿Imports BLL
Imports ENTIDADES
Imports System.Windows.Forms
Public Class FrmPersona
    Private mensaje As String = ""
    Private operacion As String = ""
    Private idPersona As Integer

    Private Sub tslIngresar_Click(sender As Object, e As EventArgs) Handles tslIngresar.Click
        gbDatos.Visible = True
        gbDatos.Enabled = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = True
        tslModificar.Enabled = False
        tslConsultar.Enabled = False
        operacion = "I"
    End Sub

    Private Sub tslModificar_Click(sender As Object, e As EventArgs) Handles tslModificar.Click
        gbDatos.Enabled = True
        gbDatos.Visible = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        btnAceptar.Enabled = True
        tslIngresar.Enabled = False
        tslConsultar.Enabled = False
        tslModificar.Enabled = True
        operacion = "M"
    End Sub

    Private Sub tslConsultar_Click(sender As Object, e As EventArgs) Handles tslConsultar.Click
        gbDatos.Visible = False
        gbDatos.Enabled = False
        gbBusqueda.Visible = True
        gbBusqueda.Enabled = True
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = False
        tslModificar.Enabled = False
        tslConsultar.Enabled = True
        btnAceptar.Enabled = False
        operacion = "C"
    End Sub

    Private Sub limpiarCampos()
        txtCedula.Text = String.Empty
        txtNombre.Text = String.Empty
        txtApellido.Text = String.Empty
        txtDireccion.Text = String.Empty
        txtTelefono.Text = String.Empty
        txtCodigo.Text = String.Empty
    End Sub

    Private Function validarCampos() As Boolean
        Dim resultado As Boolean = True
        ErrorProvider1.Clear()
        If txtCedula.Text = "" Then
            ErrorProvider1.SetError(txtCedula, Me.lblCedula.Text + "es requerido")
            resultado = False
        End If
        If txtNombre.Text = "" Then
            ErrorProvider1.SetError(txtNombre, Me.lblNombre.Text + "es requerido")
            resultado = False
        End If
        If txtApellido.Text = "" Then
            ErrorProvider1.SetError(txtApellido, Me.lblApellido.Text + "es requerido")
            resultado = False
        End If
        If txtDireccion.Text = "" Then
            ErrorProvider1.SetError(txtDireccion, Me.lblDireccion.Text + "es requerido")
            resultado = False
        End If
        If txtCodigo.Text = "" Then
            ErrorProvider1.SetError(txtCodigo, Me.lblCodigo.Text + "es requerido")
            resultado = False
        End If
        Return resultado
    End Function

    Private Function persona() As ClsPersona
        Dim objeto As ClsPersona = New ClsPersona()
        objeto.IdPersona = Me.idPersona
        objeto.Cedula = Me.txtCedula.Text.Trim
        objeto.Nombre = Me.txtNombre.Text.Trim
        objeto.Apellido = Me.txtApellido.Text.Trim
        objeto.Direccion = Me.txtDireccion.Text.Trim
        objeto.Telefono = Me.txtTelefono.Text.Trim
        objeto.Codigo = CInt(Me.txtCodigo.Text.Trim)
        Return objeto
    End Function

    Private Sub txtFiltroNombre_TextChanged(sender As Object, e As EventArgs) Handles txtFiltroNombre.TextChanged
        Dim dt As DataTable = Nothing
        Try
            dt = Bll_Persona.filtrarPorApellido(txtFiltroNombre.Text, mensaje)
            If dt.Rows.Count <> 0 Then
                dgvBusqueda.DataSource = dt
                dgvBusqueda.Columns("Id").Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvBusqueda_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBusqueda.CellDoubleClick
        Dim dr As DataGridViewRow
        Dim dt As DataTable
        Try
            dr = dgvBusqueda.Rows(e.RowIndex)
            dt = Bll_Persona.consultarPorId(dr.Cells("Id").Value, mensaje)
            Me.idPersona = dt.Rows(0)("idPersona")
            Me.txtCedula.Text = dt.Rows(0)("cedula")
            Me.txtNombre.Text = dt.Rows(0)("nombre")
            Me.txtApellido.Text = dt.Rows(0)("apellido")
            Me.txtDireccion.Text = dt.Rows(0)("direccion")
            Me.txtTelefono.Text = dt.Rows(0)("telefono")
            Me.txtCodigo.Text = dt.Rows(0)("codigo")
            Me.txtFiltroNombre.Text = String.Empty
            Me.dgvBusqueda.Columns.Clear()
            Me.tslModificar_Click(Nothing, Nothing)
        Catch ex As Exception
            Me.mensaje = "Debe dar doble click sobre la fila"
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarCampos() Then
            Select Case operacion
                Case "I"
                    If Bll_Persona.ingresarBD(persona, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
                Case "M"
                    If Bll_Persona.modificarBD(persona, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
            End Select
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
            FrmPersona_Load(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If operacion <> "" Then
            ErrorProvider1.Clear()
            gbDatos.Visible = False
            gbDatos.Enabled = False
            gbBusqueda.Visible = False
            gbBusqueda.Enabled = False
            pnlBotones.Enabled = False
            pnlBotones.Visible = False
            tslModificar.Enabled = False
            tslIngresar.Enabled = True
            tslConsultar.Enabled = True
            btnAceptar.Enabled = True
            operacion = ""
            Me.txtFiltroNombre.Text = String.Empty
            Me.dgvBusqueda.DataSource = Nothing
            limpiarCampos()
        End If
    End Sub


    Private Sub FrmPersona_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tslIngresar.Enabled = True
        tslModificar.Enabled = False
        tslConsultar.Enabled = True
        gbDatos.Visible = False
        gbBusqueda.Visible = False
        pnlBotones.Visible = False
        dgvBusqueda.ReadOnly = True
        Me.ToolTip1.SetToolTip(btnAceptar, "Aceptar")
        Me.ToolTip2.SetToolTip(btnCancelar, "Cancelar")
        dgvBusqueda.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvBusqueda.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllHeaders
    End Sub
End Class