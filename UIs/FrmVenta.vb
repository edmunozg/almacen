﻿Imports BLL
Imports ENTIDADES
Imports System.Windows.Forms
Public Class FrmVenta
    Private mensaje As String = ""
    Private operacion As String = ""
    Private idVenta As Integer
    Public externo As Integer = 0

    Private Sub tslIngresar_Click(sender As Object, e As EventArgs) Handles tslIngresar.Click
        gbDatos.Visible = True
        gbDatos.Enabled = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = True
        tslModificar.Enabled = False
        tslConsultar.Enabled = False
        operacion = "I"
    End Sub

    Public Sub tslModificar_Click(sender As Object, e As EventArgs) Handles tslModificar.Click
        gbDatos.Enabled = True
        gbDatos.Visible = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        btnAceptar.Enabled = True
        tslIngresar.Enabled = False
        tslModificar.Enabled = True
        tslConsultar.Enabled = False
        operacion = "M"
    End Sub

    Private Sub tslConsultar_Click(sender As Object, e As EventArgs) Handles tslConsultar.Click
        gbDatos.Visible = False
        gbDatos.Enabled = False
        gbBusqueda.Visible = True
        gbBusqueda.Enabled = True
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = False
        tslModificar.Enabled = False
        tslConsultar.Enabled = True
        btnAceptar.Enabled = False
        operacion = "C"
    End Sub

    Private Sub limpiarCampos()
        Me.cbAlmacen.SelectedValue = -1
        Me.dtpFecha.Value = Date.Now
        Me.cbPersona.SelectedValue = -1
        Me.nudCantidad.Value = 0
        Me.txtArticulo.Text = String.Empty
        Me.txtDetalle.Text = String.Empty
        Me.cbTipoCuenta.SelectedValue = -1
        Me.nudNotaVenta.Value = 0
        Me.nudFactura.Value = 0
        Me.nudValorTotal.Value = 0
    End Sub

    Private Function validarCampos() As Boolean
        Dim resultado As Boolean = True
        ErrorProvider1.Clear()
        If Me.cbAlmacen.SelectedValue = -1 Then
            ErrorProvider1.SetError(Me.cbAlmacen, Me.lblAlmacen.Text + "es requerido")
            resultado = False
        End If
        If Me.cbPersona.SelectedValue = -1 Then
            ErrorProvider1.SetError(Me.cbPersona, Me.lblPersona.Text + "es requerido")
            resultado = False
        End If
        If Me.nudCantidad.Value = 0 Then
            ErrorProvider1.SetError(Me.nudCantidad, Me.lblCantidad.Text + "es requerido")
            resultado = False
        End If
        If Me.txtArticulo.Text = "" Then
            ErrorProvider1.SetError(Me.txtArticulo, Me.lblArticulo.Text + "es requerido")
            resultado = False
        End If
        If Me.txtDetalle.Text = "" Then
            ErrorProvider1.SetError(Me.txtDetalle, Me.lblDetalle.Text + "es requerido")
            resultado = False
        End If
        If Me.cbTipoCuenta.SelectedValue = -1 Then
            ErrorProvider1.SetError(Me.cbTipoCuenta, Me.lblTipoCuenta.Text + "es requerido")
            resultado = False
        End If
        Return resultado
    End Function

    Private Function venta() As ClsVenta
        Dim objeto As ClsVenta = New ClsVenta()
        objeto.IdVenta = Me.idVenta
        objeto.IdAlmacen = New ClsAlmacen()
        objeto.IdAlmacen.IdAlmacen = Me.cbAlmacen.SelectedValue
        objeto.IdPersona = New ClsPersona()
        objeto.IdPersona.IdPersona = Me.cbPersona.SelectedValue
        objeto.IdTipoCuenta = New ClsTipoCuenta()
        objeto.IdTipoCuenta.IdTipoCuenta = Me.cbTipoCuenta.SelectedValue
        objeto.Fecha = Me.dtpFecha.Value
        objeto.Cantidad = Me.nudCantidad.Value
        objeto.Articulo = Me.txtArticulo.Text
        objeto.Detalle = Me.txtDetalle.Text
        objeto.NotaVenta = Me.nudNotaVenta.Value
        objeto.Factura = Me.nudFactura.Value
        objeto.ValorTotal = Me.nudValorTotal.Value
        Return objeto
    End Function

    Private Sub txtFiltro_TextChanged(sender As Object, e As EventArgs) Handles txtFiltro.TextChanged
        Dim dt As DataTable = Nothing
        Try
            dt = Bll_Venta.filtrarPorApellido(txtFiltro.Text, mensaje)
            If dt.Rows.Count <> 0 Then
                dgvBusqueda.DataSource = dt
                dgvBusqueda.Columns("Id").Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvBusqueda_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBusqueda.CellDoubleClick
        Dim dr As DataGridViewRow
        Dim dt As DataTable
        Try
            dr = dgvBusqueda.Rows(e.RowIndex)
            dt = Bll_Venta.consultarPorId(dr.Cells("Id").Value, mensaje)
            Me.idVenta = dt.Rows(0)("idVenta")
            Me.cbAlmacen.SelectedValue = dt.Rows(0)("idAlmacen")
            Me.dtpFecha.Value = dt.Rows(0)("fecha")
            Me.cbPersona.SelectedValue = dt.Rows(0)("idPersona")
            Me.nudCantidad.Value = dt.Rows(0)("cantidad")
            Me.txtArticulo.Text = dt.Rows(0)("articulo")
            Me.txtDetalle.Text = dt.Rows(0)("detalle")
            Me.cbTipoCuenta.SelectedValue = dt.Rows(0)("idTipoCuenta")
            Me.nudNotaVenta.Value = dt.Rows(0)("notaVenta")
            Me.nudFactura.Value = dt.Rows(0)("factura")
            Me.nudValorTotal.Value = dt.Rows(0)("valorTotal")
            Me.txtFiltro.Text = String.Empty
            dgvBusqueda.Columns.Clear()
            tslModificar_Click(Nothing, Nothing)
        Catch ex As Exception
            Me.mensaje = "Debe dar doble click sobre la fila"
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarCampos() Then
            Select Case operacion
                Case "I"
                    If Bll_Venta.ingresarBD(venta, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
                Case "M"
                    If Bll_Venta.modificarBD(venta, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
            End Select
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
            FrmVenta_Load(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If operacion <> "" Then
            ErrorProvider1.Clear()
            gbDatos.Visible = False
            gbDatos.Enabled = False
            gbBusqueda.Visible = False
            gbBusqueda.Enabled = False
            pnlBotones.Enabled = False
            pnlBotones.Visible = False
            tslModificar.Enabled = False
            tslIngresar.Enabled = True
            tslConsultar.Enabled = True
            btnAceptar.Enabled = True
            operacion = ""
            Me.txtFiltro.Text = String.Empty
            Me.dgvBusqueda.DataSource = Nothing
            limpiarCampos()
        End If
    End Sub


    Private Sub btnAlmacen_Click(sender As Object, e As EventArgs) Handles btnAlmacen.Click
        FrmAlmacen.ShowDialog()
        Me.cbAlmacen.DataSource = Bll_Almacen.selectBD(mensaje)
        Me.cbAlmacen.DisplayMember = "almacen"
        Me.cbAlmacen.ValueMember = "idAlmacen"
        Me.cbAlmacen.SelectedIndex = -1
    End Sub

    Private Sub btnPersona_Click(sender As Object, e As EventArgs) Handles btnPersona.Click
        FrmPersona.ShowDialog()
        Me.cbPersona.DataSource = Bll_Persona.selectBD(mensaje)
        Me.cbPersona.DisplayMember = "nombre"
        Me.cbPersona.ValueMember = "idPersona"
        Me.cbPersona.SelectedIndex = -1
    End Sub

    Private Sub btnTipoCuenta_Click(sender As Object, e As EventArgs) Handles btnTipoCuenta.Click
        FrmTipoCuenta.ShowDialog()
        Me.cbTipoCuenta.DataSource = Bll_TipoCuenta.selectBD(mensaje)
        Me.cbTipoCuenta.DisplayMember = "tipo"
        Me.cbTipoCuenta.ValueMember = "idTipoCuenta"
        Me.cbTipoCuenta.SelectedIndex = -1
    End Sub

  
    Private Sub FrmVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ToolTip1.SetToolTip(btnAceptar, "Aceptar")
        Me.ToolTip2.SetToolTip(btnCancelar, "Cancelar")
        dgvBusqueda.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvBusqueda.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllHeaders
        Me.cbAlmacen.DataSource = Bll_Almacen.selectBD(mensaje)
        Me.cbAlmacen.DisplayMember = "almacen"
        Me.cbAlmacen.ValueMember = "idAlmacen"
        Me.cbTipoCuenta.DataSource = Bll_TipoCuenta.selectBD(mensaje)
        Me.cbTipoCuenta.DisplayMember = "tipo"
        Me.cbTipoCuenta.ValueMember = "idTipoCuenta"
        Me.cbPersona.DataSource = Bll_Persona.selectBD(mensaje)
        Me.cbPersona.DisplayMember = "nombre"
        Me.cbPersona.ValueMember = "idPersona"
        If externo <> 1 Then
            Me.cbTipoCuenta.SelectedIndex = -1
            Me.cbAlmacen.SelectedIndex = -1
            Me.cbPersona.SelectedIndex = -1
        End If
        Me.tslIngresar_Click(Nothing, Nothing)
    End Sub
End Class