﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.gbDatos = New System.Windows.Forms.GroupBox()
        Me.btnAlmacen = New System.Windows.Forms.Button()
        Me.btnTipoCuenta = New System.Windows.Forms.Button()
        Me.btnPersona = New System.Windows.Forms.Button()
        Me.cbTipoCuenta = New System.Windows.Forms.ComboBox()
        Me.lblTipoCuenta = New System.Windows.Forms.Label()
        Me.cbPersona = New System.Windows.Forms.ComboBox()
        Me.lblPersona = New System.Windows.Forms.Label()
        Me.nudNotaVenta = New System.Windows.Forms.NumericUpDown()
        Me.nudCantidad = New System.Windows.Forms.NumericUpDown()
        Me.cbAlmacen = New System.Windows.Forms.ComboBox()
        Me.lblAlmacen = New System.Windows.Forms.Label()
        Me.nudValorTotal = New System.Windows.Forms.NumericUpDown()
        Me.nudFactura = New System.Windows.Forms.NumericUpDown()
        Me.lblValorTotal = New System.Windows.Forms.Label()
        Me.lblFactura = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.txtArticulo = New System.Windows.Forms.TextBox()
        Me.txtDetalle = New System.Windows.Forms.TextBox()
        Me.lblNotaVenta = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.lblArticulo = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.gbBusqueda = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFiltro = New System.Windows.Forms.TextBox()
        Me.dgvBusqueda = New System.Windows.Forms.DataGridView()
        Me.pnlBotones = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tslIngresar = New System.Windows.Forms.ToolStripLabel()
        Me.tslModificar = New System.Windows.Forms.ToolStripLabel()
        Me.tslConsultar = New System.Windows.Forms.ToolStripLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.gbDatos.SuspendLayout()
        CType(Me.nudNotaVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudValorTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBusqueda.SuspendLayout()
        CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBotones.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.gbDatos)
        Me.FlowLayoutPanel1.Controls.Add(Me.gbBusqueda)
        Me.FlowLayoutPanel1.Controls.Add(Me.pnlBotones)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 23)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(712, 601)
        Me.FlowLayoutPanel1.TabIndex = 11
        '
        'gbDatos
        '
        Me.gbDatos.Controls.Add(Me.btnAlmacen)
        Me.gbDatos.Controls.Add(Me.btnTipoCuenta)
        Me.gbDatos.Controls.Add(Me.btnPersona)
        Me.gbDatos.Controls.Add(Me.cbTipoCuenta)
        Me.gbDatos.Controls.Add(Me.lblTipoCuenta)
        Me.gbDatos.Controls.Add(Me.cbPersona)
        Me.gbDatos.Controls.Add(Me.lblPersona)
        Me.gbDatos.Controls.Add(Me.nudNotaVenta)
        Me.gbDatos.Controls.Add(Me.nudCantidad)
        Me.gbDatos.Controls.Add(Me.cbAlmacen)
        Me.gbDatos.Controls.Add(Me.lblAlmacen)
        Me.gbDatos.Controls.Add(Me.nudValorTotal)
        Me.gbDatos.Controls.Add(Me.nudFactura)
        Me.gbDatos.Controls.Add(Me.lblValorTotal)
        Me.gbDatos.Controls.Add(Me.lblFactura)
        Me.gbDatos.Controls.Add(Me.dtpFecha)
        Me.gbDatos.Controls.Add(Me.txtArticulo)
        Me.gbDatos.Controls.Add(Me.txtDetalle)
        Me.gbDatos.Controls.Add(Me.lblNotaVenta)
        Me.gbDatos.Controls.Add(Me.lblDetalle)
        Me.gbDatos.Controls.Add(Me.lblArticulo)
        Me.gbDatos.Controls.Add(Me.lblFecha)
        Me.gbDatos.Controls.Add(Me.lblCantidad)
        Me.gbDatos.Location = New System.Drawing.Point(3, 3)
        Me.gbDatos.Name = "gbDatos"
        Me.gbDatos.Size = New System.Drawing.Size(701, 390)
        Me.gbDatos.TabIndex = 2
        Me.gbDatos.TabStop = False
        Me.gbDatos.Text = "Datos"
        '
        'btnAlmacen
        '
        Me.btnAlmacen.Image = Global.UIs.My.Resources.Resources.Apps_preferences_system_windows_icon
        Me.btnAlmacen.Location = New System.Drawing.Point(290, 25)
        Me.btnAlmacen.Name = "btnAlmacen"
        Me.btnAlmacen.Size = New System.Drawing.Size(25, 25)
        Me.btnAlmacen.TabIndex = 12
        Me.btnAlmacen.UseVisualStyleBackColor = True
        '
        'btnTipoCuenta
        '
        Me.btnTipoCuenta.Image = Global.UIs.My.Resources.Resources.Apps_preferences_system_windows_icon
        Me.btnTipoCuenta.Location = New System.Drawing.Point(290, 237)
        Me.btnTipoCuenta.Name = "btnTipoCuenta"
        Me.btnTipoCuenta.Size = New System.Drawing.Size(25, 25)
        Me.btnTipoCuenta.TabIndex = 25
        Me.btnTipoCuenta.UseVisualStyleBackColor = True
        '
        'btnPersona
        '
        Me.btnPersona.Image = Global.UIs.My.Resources.Resources.Apps_preferences_system_windows_icon
        Me.btnPersona.Location = New System.Drawing.Point(460, 97)
        Me.btnPersona.Name = "btnPersona"
        Me.btnPersona.Size = New System.Drawing.Size(25, 25)
        Me.btnPersona.TabIndex = 24
        Me.btnPersona.UseVisualStyleBackColor = True
        '
        'cbTipoCuenta
        '
        Me.cbTipoCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTipoCuenta.FormattingEnabled = True
        Me.cbTipoCuenta.Location = New System.Drawing.Point(112, 240)
        Me.cbTipoCuenta.Name = "cbTipoCuenta"
        Me.cbTipoCuenta.Size = New System.Drawing.Size(172, 21)
        Me.cbTipoCuenta.TabIndex = 6
        '
        'lblTipoCuenta
        '
        Me.lblTipoCuenta.AutoSize = True
        Me.lblTipoCuenta.Location = New System.Drawing.Point(30, 247)
        Me.lblTipoCuenta.Name = "lblTipoCuenta"
        Me.lblTipoCuenta.Size = New System.Drawing.Size(79, 13)
        Me.lblTipoCuenta.TabIndex = 22
        Me.lblTipoCuenta.Text = "Tipo de cuenta"
        '
        'cbPersona
        '
        Me.cbPersona.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbPersona.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbPersona.DropDownHeight = 300
        Me.cbPersona.FormattingEnabled = True
        Me.cbPersona.IntegralHeight = False
        Me.cbPersona.ItemHeight = 13
        Me.cbPersona.Location = New System.Drawing.Point(113, 99)
        Me.cbPersona.MaxDropDownItems = 10
        Me.cbPersona.Name = "cbPersona"
        Me.cbPersona.Size = New System.Drawing.Size(341, 21)
        Me.cbPersona.TabIndex = 2
        '
        'lblPersona
        '
        Me.lblPersona.AutoSize = True
        Me.lblPersona.Location = New System.Drawing.Point(49, 103)
        Me.lblPersona.Name = "lblPersona"
        Me.lblPersona.Size = New System.Drawing.Size(46, 13)
        Me.lblPersona.TabIndex = 20
        Me.lblPersona.Text = "Persona"
        '
        'nudNotaVenta
        '
        Me.nudNotaVenta.Location = New System.Drawing.Point(112, 276)
        Me.nudNotaVenta.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.nudNotaVenta.Name = "nudNotaVenta"
        Me.nudNotaVenta.Size = New System.Drawing.Size(120, 20)
        Me.nudNotaVenta.TabIndex = 7
        '
        'nudCantidad
        '
        Me.nudCantidad.Location = New System.Drawing.Point(112, 135)
        Me.nudCantidad.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.nudCantidad.Name = "nudCantidad"
        Me.nudCantidad.Size = New System.Drawing.Size(121, 20)
        Me.nudCantidad.TabIndex = 3
        '
        'cbAlmacen
        '
        Me.cbAlmacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbAlmacen.FormattingEnabled = True
        Me.cbAlmacen.Location = New System.Drawing.Point(112, 28)
        Me.cbAlmacen.Name = "cbAlmacen"
        Me.cbAlmacen.Size = New System.Drawing.Size(172, 21)
        Me.cbAlmacen.TabIndex = 0
        '
        'lblAlmacen
        '
        Me.lblAlmacen.AutoSize = True
        Me.lblAlmacen.Location = New System.Drawing.Point(41, 31)
        Me.lblAlmacen.Name = "lblAlmacen"
        Me.lblAlmacen.Size = New System.Drawing.Size(48, 13)
        Me.lblAlmacen.TabIndex = 16
        Me.lblAlmacen.Text = "Almacen"
        '
        'nudValorTotal
        '
        Me.nudValorTotal.DecimalPlaces = 2
        Me.nudValorTotal.Location = New System.Drawing.Point(113, 349)
        Me.nudValorTotal.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.nudValorTotal.Name = "nudValorTotal"
        Me.nudValorTotal.Size = New System.Drawing.Size(120, 20)
        Me.nudValorTotal.TabIndex = 9
        '
        'nudFactura
        '
        Me.nudFactura.Location = New System.Drawing.Point(113, 311)
        Me.nudFactura.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.nudFactura.Name = "nudFactura"
        Me.nudFactura.Size = New System.Drawing.Size(120, 20)
        Me.nudFactura.TabIndex = 8
        '
        'lblValorTotal
        '
        Me.lblValorTotal.AutoSize = True
        Me.lblValorTotal.Location = New System.Drawing.Point(41, 355)
        Me.lblValorTotal.Name = "lblValorTotal"
        Me.lblValorTotal.Size = New System.Drawing.Size(54, 13)
        Me.lblValorTotal.TabIndex = 12
        Me.lblValorTotal.Text = "Valor total"
        '
        'lblFactura
        '
        Me.lblFactura.AutoSize = True
        Me.lblFactura.Location = New System.Drawing.Point(52, 319)
        Me.lblFactura.Name = "lblFactura"
        Me.lblFactura.Size = New System.Drawing.Size(43, 13)
        Me.lblFactura.TabIndex = 11
        Me.lblFactura.Text = "Factura"
        '
        'dtpFecha
        '
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(113, 64)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(120, 20)
        Me.dtpFecha.TabIndex = 1
        '
        'txtArticulo
        '
        Me.txtArticulo.Location = New System.Drawing.Point(113, 170)
        Me.txtArticulo.Name = "txtArticulo"
        Me.txtArticulo.Size = New System.Drawing.Size(341, 20)
        Me.txtArticulo.TabIndex = 4
        '
        'txtDetalle
        '
        Me.txtDetalle.Location = New System.Drawing.Point(113, 205)
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(341, 20)
        Me.txtDetalle.TabIndex = 5
        '
        'lblNotaVenta
        '
        Me.lblNotaVenta.AutoSize = True
        Me.lblNotaVenta.Location = New System.Drawing.Point(20, 283)
        Me.lblNotaVenta.Name = "lblNotaVenta"
        Me.lblNotaVenta.Size = New System.Drawing.Size(75, 13)
        Me.lblNotaVenta.TabIndex = 5
        Me.lblNotaVenta.Text = "Nota de venta"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(55, 211)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(40, 13)
        Me.lblDetalle.TabIndex = 4
        Me.lblDetalle.Text = "Detalle"
        '
        'lblArticulo
        '
        Me.lblArticulo.AutoSize = True
        Me.lblArticulo.Location = New System.Drawing.Point(53, 175)
        Me.lblArticulo.Name = "lblArticulo"
        Me.lblArticulo.Size = New System.Drawing.Size(42, 13)
        Me.lblArticulo.TabIndex = 3
        Me.lblArticulo.Text = "Articulo"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(58, 67)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(37, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(46, 139)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(49, 13)
        Me.lblCantidad.TabIndex = 1
        Me.lblCantidad.Text = "Cantidad"
        '
        'gbBusqueda
        '
        Me.gbBusqueda.Controls.Add(Me.Label2)
        Me.gbBusqueda.Controls.Add(Me.txtFiltro)
        Me.gbBusqueda.Controls.Add(Me.dgvBusqueda)
        Me.gbBusqueda.Location = New System.Drawing.Point(3, 399)
        Me.gbBusqueda.Name = "gbBusqueda"
        Me.gbBusqueda.Size = New System.Drawing.Size(701, 400)
        Me.gbBusqueda.TabIndex = 3
        Me.gbBusqueda.TabStop = False
        Me.gbBusqueda.Text = "Busqueda "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Filtrar por Apellido"
        '
        'txtFiltro
        '
        Me.txtFiltro.Location = New System.Drawing.Point(142, 18)
        Me.txtFiltro.Name = "txtFiltro"
        Me.txtFiltro.Size = New System.Drawing.Size(312, 20)
        Me.txtFiltro.TabIndex = 5
        '
        'dgvBusqueda
        '
        Me.dgvBusqueda.AllowUserToAddRows = False
        Me.dgvBusqueda.AllowUserToDeleteRows = False
        Me.dgvBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBusqueda.Location = New System.Drawing.Point(6, 56)
        Me.dgvBusqueda.Name = "dgvBusqueda"
        Me.dgvBusqueda.Size = New System.Drawing.Size(689, 338)
        Me.dgvBusqueda.TabIndex = 4
        '
        'pnlBotones
        '
        Me.pnlBotones.Controls.Add(Me.btnCancelar)
        Me.pnlBotones.Controls.Add(Me.btnAceptar)
        Me.pnlBotones.Location = New System.Drawing.Point(3, 805)
        Me.pnlBotones.Name = "pnlBotones"
        Me.pnlBotones.Size = New System.Drawing.Size(701, 40)
        Me.pnlBotones.TabIndex = 6
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(444, 8)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(200, 8)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 10
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tslIngresar, Me.tslModificar, Me.tslConsultar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(716, 25)
        Me.ToolStrip1.TabIndex = 10
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tslIngresar
        '
        Me.tslIngresar.Image = Global.UIs.My.Resources.Resources.ingresar
        Me.tslIngresar.Name = "tslIngresar"
        Me.tslIngresar.Size = New System.Drawing.Size(65, 22)
        Me.tslIngresar.Text = "Ingresar"
        '
        'tslModificar
        '
        Me.tslModificar.Image = Global.UIs.My.Resources.Resources.modificar
        Me.tslModificar.Name = "tslModificar"
        Me.tslModificar.Size = New System.Drawing.Size(74, 22)
        Me.tslModificar.Text = "Modificar"
        '
        'tslConsultar
        '
        Me.tslConsultar.Image = Global.UIs.My.Resources.Resources.consultar
        Me.tslConsultar.Name = "tslConsultar"
        Me.tslConsultar.Size = New System.Drawing.Size(74, 22)
        Me.tslConsultar.Text = "Consultar"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'FrmVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 488)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "FrmVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ".:: Venta ::."
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.gbDatos.ResumeLayout(False)
        Me.gbDatos.PerformLayout()
        CType(Me.nudNotaVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudValorTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFactura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBusqueda.ResumeLayout(False)
        Me.gbBusqueda.PerformLayout()
        CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBotones.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbDatos As System.Windows.Forms.GroupBox
    Friend WithEvents cbTipoCuenta As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoCuenta As System.Windows.Forms.Label
    Friend WithEvents cbPersona As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersona As System.Windows.Forms.Label
    Friend WithEvents nudNotaVenta As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudCantidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbAlmacen As System.Windows.Forms.ComboBox
    Friend WithEvents lblAlmacen As System.Windows.Forms.Label
    Friend WithEvents nudValorTotal As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudFactura As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblValorTotal As System.Windows.Forms.Label
    Friend WithEvents lblFactura As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtArticulo As System.Windows.Forms.TextBox
    Friend WithEvents txtDetalle As System.Windows.Forms.TextBox
    Friend WithEvents lblNotaVenta As System.Windows.Forms.Label
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents lblArticulo As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents gbBusqueda As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFiltro As System.Windows.Forms.TextBox
    Friend WithEvents dgvBusqueda As System.Windows.Forms.DataGridView
    Friend WithEvents pnlBotones As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tslIngresar As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tslModificar As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tslConsultar As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnTipoCuenta As System.Windows.Forms.Button
    Friend WithEvents btnPersona As System.Windows.Forms.Button
    Friend WithEvents btnAlmacen As System.Windows.Forms.Button
End Class
