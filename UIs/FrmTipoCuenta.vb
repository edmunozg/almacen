﻿Imports BLL
Imports ENTIDADES
Imports System.Windows.Forms
Public Class FrmTipoCuenta
    Private mensaje As String = ""
    Private operacion As String = ""
    Private idTipoCuenta As Integer

    Private Sub tslIngresar_Click(sender As Object, e As EventArgs) Handles tslIngresar.Click
        gbDatos.Visible = True
        gbDatos.Enabled = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = False
        tslModificar.Enabled = False
        tslConsultar.Enabled = False
        operacion = "I"
    End Sub

    Private Sub tslModificar_Click(sender As Object, e As EventArgs) Handles tslModificar.Click
        gbDatos.Enabled = True
        gbDatos.Visible = True
        gbBusqueda.Visible = False
        gbBusqueda.Enabled = False
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        btnAceptar.Enabled = True
        tslIngresar.Enabled = False
        tslConsultar.Enabled = False
        tslModificar.Enabled = True
        operacion = "M"
    End Sub

    Private Sub tslConsultar_Click(sender As Object, e As EventArgs) Handles tslConsultar.Click
        gbDatos.Visible = False
        gbDatos.Enabled = False
        gbBusqueda.Visible = True
        gbBusqueda.Enabled = True
        pnlBotones.Enabled = True
        pnlBotones.Visible = True
        tslIngresar.Enabled = False
        tslModificar.Enabled = False
        tslConsultar.Enabled = False
        btnAceptar.Enabled = False
        operacion = "C"
    End Sub

    Private Sub limpiarCampos()
        txtTipo.Text = String.Empty
    End Sub

    Private Function validarCampos() As Boolean
        Dim resultado As Boolean = True
        ErrorProvider1.Clear()
        If txtTipo.Text = "" Then
            ErrorProvider1.SetError(txtTipo, Me.lblTipo.Text + "es requerido")
            resultado = False
        End If
        Return resultado
    End Function

    Private Function tipoCuenta() As ClsTipoCuenta
        Dim objeto As ClsTipoCuenta = New ClsTipoCuenta()
        objeto.IdTipoCuenta = Me.idTipoCuenta
        objeto.Tipo = Me.txtTipo.Text.Trim
        Return objeto
    End Function

    Private Sub txtFiltro_TextChanged(sender As Object, e As EventArgs) Handles txtFiltro.TextChanged
        Dim dt As DataTable = Nothing
        Try
            dt = Bll_TipoCuenta.filtrarPorTipo(Me.txtFiltro.Text, mensaje)
            If dt.Rows.Count <> 0 Then
                Me.dgvBusqueda.DataSource = dt
                Me.dgvBusqueda.Columns("Id").Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvBusqueda_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBusqueda.CellDoubleClick
        Dim dr As DataGridViewRow
        Dim dt As DataTable
        Try
            dr = dgvBusqueda.Rows(e.RowIndex)
            dt = Bll_TipoCuenta.consultarPorId(dr.Cells("Id").Value, mensaje)
            Me.idTipoCuenta = dt.Rows(0)("idTipoCuenta")
            Me.txtTipo.Text = dt.Rows(0)("tipo")
            Me.txtFiltro.Text = String.Empty
            Me.dgvBusqueda.Columns.Clear()
            Me.tslModificar_Click(Nothing, Nothing)
        Catch ex As Exception
            Me.mensaje = "Debe dar doble click sobre la fila"
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarCampos() Then
            Select Case operacion
                Case "I"
                    If Bll_TipoCuenta.ingresarBD(tipoCuenta, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
                Case "M"
                    If Bll_TipoCuenta.modificarBD(tipoCuenta, mensaje) Then
                        limpiarCampos()
                        btnCancelar_Click(Nothing, Nothing)
                    End If
            End Select
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
            FrmTipoCuenta_Load(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If operacion <> "" Then
            ErrorProvider1.Clear()
            gbDatos.Visible = False
            gbDatos.Enabled = False
            gbBusqueda.Visible = False
            gbBusqueda.Enabled = False
            pnlBotones.Enabled = False
            pnlBotones.Visible = False
            tslModificar.Enabled = False
            tslIngresar.Enabled = True
            tslConsultar.Enabled = True
            btnAceptar.Enabled = True
            operacion = ""
            Me.txtFiltro.Text = String.Empty
            Me.dgvBusqueda.DataSource = Nothing
            limpiarCampos()
        End If
    End Sub

    Private Sub FrmTipoCuenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tslIngresar.Enabled = True
        tslModificar.Enabled = False
        tslConsultar.Enabled = True
        gbDatos.Visible = False
        gbBusqueda.Visible = False
        pnlBotones.Visible = False
        dgvBusqueda.ReadOnly = True
        Me.ToolTip1.SetToolTip(btnAceptar, "Aceptar")
        Me.ToolTip2.SetToolTip(btnCancelar, "Cancelar")
        dgvBusqueda.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dgvBusqueda.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllHeaders
    End Sub
End Class