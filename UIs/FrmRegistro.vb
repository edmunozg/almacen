﻿Imports BLL
Public Class FrmRegistro
    Private mensaje As String = ""
    Private Sub FrmRegistro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.dgvRegistro.AllowUserToAddRows = False
        Me.dgvRegistro.AllowUserToDeleteRows = False
        Me.dgvRegistro.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvRegistro.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllHeaders
        Dim dt As DataTable = Nothing
        Try
            dt = Bll_Registro.registros(mensaje)
            If dt.Rows.Count <> 0 Then
                Me.dgvRegistro.DataSource = dt
                Me.dgvRegistro.Columns("Id").Visible = False
                Me.dgvRegistro.Columns("idAlmacen").Visible = False
                Me.dgvRegistro.Columns("idPersona").Visible = False
                Me.dgvRegistro.Columns("idTipoCuenta").Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub


    Private Sub txtFiltro_TextChanged(sender As Object, e As EventArgs) Handles txtFiltro.TextChanged
        Dim dt As DataTable = Nothing
        Try
            If (Me.rbCodigo.Checked) Then
                dt = Bll_Registro.filtrarPorCodigo(Me.txtFiltro.Text, Me.mensaje)
            ElseIf (Me.rbCedula.Checked) Then
                dt = Bll_Registro.filtrarPorCedula(Me.txtFiltro.Text, Me.mensaje)
            ElseIf (Me.rbApellido.Checked) Then
                dt = Bll_Registro.filtrarPorApellido(Me.txtFiltro.Text, Me.mensaje)
            Else
                MsgBox("Seleccione una opción de filtro", MsgBoxStyle.Information, My.Settings.NOMBREAPP)
            End If
            If dt.Rows.Count <> 0 Then
                Me.dgvRegistro.DataSource = dt
                Me.dgvRegistro.Columns("Id").Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub soloLectura()
        FrmVenta.cbAlmacen.Enabled = False
        FrmVenta.dtpFecha.Enabled = False
        FrmVenta.cbPersona.Enabled = False
        FrmVenta.nudCantidad.Enabled = False
        FrmVenta.txtArticulo.Enabled = False
        FrmVenta.txtDetalle.Enabled = False
        FrmVenta.cbTipoCuenta.Enabled = False
        FrmVenta.nudNotaVenta.Enabled = False
        FrmVenta.nudFactura.Enabled = False
        FrmVenta.nudValorTotal.Enabled = False
        FrmVenta.btnAlmacen.Enabled = False
        FrmVenta.btnPersona.Enabled = False
        FrmVenta.btnTipoCuenta.Enabled = False
        FrmVenta.MaximizeBox = False
        FrmVenta.MinimizeBox = False
    End Sub


    Private Sub dgvRegistro_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRegistro.CellDoubleClick
        Dim dr As DataGridViewRow
        Dim dt As DataTable
        Try
            dr = Me.dgvRegistro.Rows(e.RowIndex)
            'dt = Bll_Venta.consultarPorId(dr.Cells("Id").Value, mensaje)
            'FrmVenta.tslModificar_Click(Nothing, Nothing)
            FrmVenta.cbAlmacen.SelectedValue = dr.Cells("idAlmacen").Value
            FrmVenta.dtpFecha.Value = dr.Cells("FECHA").Value
            FrmVenta.cbPersona.SelectedValue = dr.Cells("idPersona").Value
            FrmVenta.nudCantidad.Value = dr.Cells("CANTIDAD").Value
            FrmVenta.txtArticulo.Text = dr.Cells("ARTICULO").Value
            FrmVenta.txtDetalle.Text = dr.Cells("DETALLE").Value
            FrmVenta.cbTipoCuenta.SelectedValue = dr.Cells("idTipoCuenta").Value
            FrmVenta.nudNotaVenta.Value = dr.Cells("NOTA_VENTA").Value
            FrmVenta.nudFactura.Value = dr.Cells("FACTURA").Value
            FrmVenta.nudValorTotal.Value = dr.Cells("VALOR_TOTAL").Value
            FrmVenta.btnAceptar.Visible = False
            FrmVenta.btnCancelar.Visible = False
            soloLectura()
            FrmVenta.externo = 1
            FrmVenta.ShowDialog()
        Catch ex As Exception
            Me.mensaje = "Debe dar doble click sobre la fila"
            MsgBox(mensaje, MsgBoxStyle.Information, My.Settings.NOMBREAPP)
        End Try
    End Sub
End Class