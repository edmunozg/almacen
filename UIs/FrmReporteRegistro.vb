﻿Imports BLL
Imports Microsoft.Reporting.WinForms

Public Class FrmReporteRegistro
    Private mensaje As String = ""
    'Private Sub FrmReporteRegistro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    '    Me.rvRegistro.RefreshReport()
    'End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        Dim ds As DataSet = Bll_Reporte.reporteRegistroPorFecha(Me.dtpFechaInicio.Value, Me.dtpFechaFin.Value, Me.mensaje)
        '----------------------------------------------------------
        Dim rptDSet = New ReportDataSource
        rptDSet.Name = "DataSet1"
        rptDSet.Value = ds.Tables(0)
        Me.rvRegistro.Clear()
        Me.rvRegistro.ProcessingMode = ProcessingMode.Local
        Me.rvRegistro.LocalReport.ReportPath = "C:\Users\USUARIO\Documents\Bitbutket\Almacen\UIs\reportRegistro.rdlc"
        Me.rvRegistro.LocalReport.DataSources.Clear()
        Me.rvRegistro.LocalReport.DataSources.Add(rptDSet)
        Me.rvRegistro.DocumentMapCollapsed = True
        Try
            Dim myPar1 As ReportParameter = New ReportParameter("rpFechaInicio", FormatDateTime(Me.dtpFechaInicio.Value, DateFormat.ShortDate))
            Dim myPar2 As ReportParameter = New ReportParameter("rpFechaFin", FormatDateTime(Me.dtpFechaFin.Value, DateFormat.ShortDate))
            Me.rvRegistro.LocalReport.SetParameters(myPar1)
            Me.rvRegistro.LocalReport.SetParameters(myPar2)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.rvRegistro.ZoomMode = ZoomMode.PageWidth
        Me.rvRegistro.RefreshReport()
    End Sub
End Class